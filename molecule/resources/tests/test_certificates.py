import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("default_group")


def test_certificate_files(host):
    fqdn = host.run("hostname --fqdn").stdout.strip()
    if not host.ansible.get_variables()["inventory_hostname"].startswith("ubuntu"):
        assert host.file("/etc/pki/tls/private/" + fqdn + ".key").exists
        assert host.file("/etc/pki/tls/certs/" + fqdn + ".crt").exists
        assert host.file("/etc/pki/tls/certs/" + fqdn + "-chained.crt").exists
    elif host.ansible.get_variables()["inventory_hostname"].startswith("ubuntu"):
        assert host.file("/etc/ssl/private/" + fqdn + ".key").exists
        assert host.file("/etc/ssl/certs/" + fqdn + ".crt").exists
        assert host.file("/etc/ssl/certs/" + fqdn + "-chained.crt").exists


def test_certificate_hostname(host):
    if not host.ansible.get_variables()["inventory_hostname"].startswith("ubuntu"):
        host.run("yum -y install openssl")
        fqdn = host.run("hostname --fqdn").stdout.strip()
        check = host.run("openssl x509 -checkhost " + fqdn + " -noout -in /etc/pki/tls/certs/" + fqdn + ".crt")
        assert check.stdout.strip() == "Hostname " + fqdn + " does match certificate"
    elif host.ansible.get_variables()["inventory_hostname"].startswith("ubuntu"):
        host.run("apt -y install openssl")
        fqdn = host.run("hostname --fqdn").stdout.strip()
        check = host.run("openssl x509 -checkhost " + fqdn + " -noout -in /etc/ssl/certs/" + fqdn + ".crt")
        assert check.stdout.strip() == "Hostname " + fqdn + " does match certificate"
