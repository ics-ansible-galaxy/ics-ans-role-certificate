#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
import os
import requests
from requests_ntlm import HttpNtlmAuth
import re
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.x509.oid import ExtensionOID
from cryptography.x509.extensions import ExtensionNotFound


def main():
    module = AnsibleModule(
        argument_spec=dict(
            url=dict(required=True),
            username=dict(required=True),
            password=dict(no_log=True, required=True),
            csr=dict(required=True),
            attributes=dict(),
            certificate=dict(required=True),
            certificate_chain=dict(required=True),
            verify=dict(default=True, type="bool"),
            force=dict(default=False, type="bool"),
        )
    )
    with open(module.params["csr"], "rb") as csr_file:
        csr_content = csr_file.read()
    # Check if new enrollment is needed
    if (
        not module.params["force"]
        and os.path.isfile(module.params["certificate"])
        and os.path.isfile(module.params["certificate_chain"])
    ):
        csr = x509.load_pem_x509_csr(csr_content, default_backend())
        cert = x509.load_pem_x509_certificate(
            open(module.params["certificate"]).read().encode('ascii'), default_backend()
        )
        if csr.subject == cert.subject:
            try:
                csr_subject_alternative_names = csr.extensions.get_extension_for_oid(
                    ExtensionOID.SUBJECT_ALTERNATIVE_NAME
                )
            except ExtensionNotFound:
                csr_subject_alternative_names = None
            try:
                cert_subject_alternative_names = cert.extensions.get_extension_for_oid(
                    ExtensionOID.SUBJECT_ALTERNATIVE_NAME
                )
            except ExtensionNotFound:
                cert_subject_alternative_names = None
            if csr_subject_alternative_names == cert_subject_alternative_names:
                module.exit_json(changed=False)

    s = requests.Session()
    r = s.post(
        module.params["url"] + "certfnsh.asp",
        auth=HttpNtlmAuth(module.params["username"], module.params["password"]),
        data={
            "Mode": "newreq",
            "CertRequest": csr_content,
            "CertAttrib": module.params["attributes"],
            "TargetStoreFlags": "0",
            "SaveCert": "yes",
            "ThumbPrint": "",
        },
        verify=module.params["verify"],
    )
    cert_url = (
        module.params["url"]
        + re.search(r"certnew.cer\?ReqID\=(\d)+", r.text).group(0)
        + "&Enc=b64"
    )
    chain_url = (
        module.params["url"]
        + re.search(r"certnew.p7b\?ReqID\=(\d)+", r.text).group(0)
        + "&Enc=b64"
    )
    r = s.get(cert_url)
    open(module.params["certificate"], "wb").write(r.content)
    r = s.get(chain_url)
    open(module.params["certificate_chain"], "wb").write(r.content)

    module.exit_json(changed=True)


if __name__ == "__main__":
    main()
