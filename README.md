# ics-ans-role-certificate

Ansible role to install an ESS certificate and key.

This role has 3 ways of supplying a server certificate:

- If variables "certificate_key" and "certificate_certificate" are set:
  Use content of these variables
- If ansible_domain is within the defined list:
  Enroll host specific certificate from internal CA
- If ansible_domain is not one of the above:
  Create a self-signed certificate

## Requirements

- ansible >= 2.7
- molecule >= 2.20

## Role Variables

```yaml
certificate_key_path: "/etc/pki/tls/private/{{ ansible_fqdn }}.key"
certificate_cert_path: "/etc/pki/tls/certs/{{ ansible_fqdn }}.crt"
certificate_cert_chain_path: "/etc/pki/tls/certs/{{ ansible_fqdn }}-chained.crt"

certificate_country: SE
certificate_state: Skane
certificate_locality: Lund
certificate_organisation: ESSS
certificate_cn: "{{ ansible_fqdn }}"
certificate_alt_names: []
certificate_subject: "/C={{ certificate_country }}/ST={{ certificate_state }}/L={{ certificate_locality }}/O={{ certificate_organisation }}/CN={{ certificate_cn }}"
certificate_bits: 2048

certificate_csr_path: "/etc/pki/tls/{{ ansible_fqdn }}.csr"
certificate_cnf_path: "/etc/pki/tls/{{ ansible_fqdn }}.cnf"
certificate_enrollment_domains:
  - tn.esss.lu.se
  - cslab.esss.lu.se
  - esss.lu.se

certificate_adcs_url: https://ca01.esss.lu.se/certsrv/
certificate_adcs_username: srv_ansible
certificate_adcs_password: ''
certificate_adcs_attributes: CertificateTemplate:Computer(2048bits5yrLinux)
certificate_adcs_ssl_verify: true
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-certificate
```

## License

BSD 2-clause
